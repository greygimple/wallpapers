#!/usr/bin/env python3
import os

RandomSelectPath = "/home/grey/Pictures/wallpapers/Random-Select/"
files = os.listdir(RandomSelectPath)

# start counting at 1
count = 1

# iterate over every wallpaper
for file in files:
    # create an empty string to store the final path
    finalPath = ""
    # number of digits in the count of the file
    digitsCount = len(str(count))
    # add leading zeros
    # running i times where i = 4 - the number of digits in the file count
    for i in range(4-digitsCount):
        finalPath += "0"
    # add the count and file extention to the finalPath
    finalPath += str(count) + ".jpg"
    # increase the count
    count += 1
    # rename the file
    os.rename(RandomSelectPath + file, RandomSelectPath + finalPath)
    # print a message to the terminal stating the change
    print(f"File: {file}\nUpdated: {finalPath}")
